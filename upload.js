// runs as soon as doc is ready for manipulation
$(document).ready(function() {

	  alert("Welcome to SEEKR!");
    onready();
    upload();

});

function onready(){
  // functionality for greeting button
  $("button.greet").click( function(event) {
    var greet_div = $("div.greet");
    greet_div.append("Hey! ");

    $.ajax({
      type: "GET",
      url: "http://localhost:5000",
      dataType: "",
      success: function(){
        alert("success!");
      },
      error: function (xhr, ajaxOptions, thrownError) {
      alert(xhr.status);
      alert(thrownError);
    }

    });

  });
};

function upload() {
  $('#uploadfile').click(
    function(event){

      postAjax();
    }
  );
}

function postAjax() {

  $.ajax({
    type: "POST",
    url: "http://localhost:5000/uploadFile",
    dataType: "json",
    success: function(){
      alert("success!");
    },
    error: function (xhr, ajaxOptions, thrownError) {
    alert(xhr.status);
    alert(thrownError);
  }

  });
}
